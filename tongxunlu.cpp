#include <stdio.h>
#include<string.h>
#include <stdlib.h>
#define MAX 50
int n = 0, residue= MAX;//n是剩余人数 ，residue是存放多少人 
typedef struct man//定义结构来表示编号，姓名，电话 
{
	int num;
	char name[20];
	char phone[20];
}list;
typedef struct men
{
	struct man list[MAX];
}men;
void test();
void menu();
int find(men* p, int num);
void add(men* p);
void dele(men* p);
void search(men* p);
void change(men* p);
void sort_name(men* p);
struct men book;
int main()
{
	menu();
	system("pause");
	return 0;
}
void menu()
{
	int opt = 0;
	do
	{
		test();
		printf("请输入操作：");
		scanf("%d", &opt);
		switch (opt)
		{
		test();
		case 1:
			sort_name(&book);
			system("cls");
			break;
		case 2:
			add(&book);
			system("cls");
			break;
		case 3:
			dele(&book);
			system("cls");
			break;
		case 4:
			change(&book);
			system("cls");
			break;
		case 5:
			search(&book);
			system("cls");
			break;
		case 6:
			exit(0);
			break;
		default:
			printf("Error!!!\n错误操作指令, 请重新输入");
			break;
		}
	} while (opt != 6);

}
void test()
{
	printf("========== 通讯录 ==========\n\n\n");
	printf("========== 界面 ==========\n");
	printf("人数：%d	|剩余空间：%d\n", n, residue);
	for (int i=0; i < 50; i++)
	{
		if(book.list[i].num!=0){//只要这个电话簿还有人 
			printf("编号：		%2d| 姓名：		%s|", book.list[i].num, book.list[i].name);
			printf("电话：		%s\n", book.list[i].phone);		
		}
	}
	system("pause");
	printf("\n\n\n操作列表:\n");
	printf("1)排序          2)添加          3)删除\n4)修改          5)查找          6)退出程序\n\n\n\n");
}
int find(men* p, int num)
{
	if (num > 50 || num < 1) {
		printf("处理编号超过阈值");
		system("pause");
	}
	int i = 0;
	for (; i <= MAX; i++)
	{
		if (p->list[i].num == num)
		{
			return i;
		}
	}
	return -1;
}
void dele(men* p)
{
	int s;
	int ret;
	printf("请输入操作位置：");
	scanf("%d", &s);
	ret = find(p, s);
	if (ret == -1)
	{
		printf("此位置没有数据\n");
		system("pause");
	}
	else
	{
		int j = ret;
		for (; j < 50; j++)
		{
			p->list[j] = p->list[j + 1];
		}
		printf("已删除！！！\n");
		n--;
		residue++;
	}
}
void add(men* p)
{
	int j = 0;
	printf("请输入添加位置：");
	scanf("%d", &j);
	if (j > 50 || j < 1) {
		printf("处理编号超过阈值");
		system("pause");
	}
	else if (p->list[j].num != 0)
	{
		printf("此处已有数据");
		system("pause");
	}
	else
	{
		p->list[j].num = j;
		printf("请输入联系人姓名：");
		scanf("%s", p->list[j].name);
		printf("请输入联系人电话：");
		scanf("%s", &p->list[j].phone);
		n++;
		residue--;
	}

}
void search(men* p)
{
	char name[20];
	int q =0;
	printf("请输入要查询的联系人：");
	scanf("%s", &name);
	for ( int i = 0; i <= 50; i++)
	{
		if (strcmp(p->list[i].name, name) == 0)
		{
			getchar();
			printf("编号：		%2d | 姓名：		%s |", p->list[i].num, p->list[i].name);
			printf("电话：		%s\n", p->list[i].phone);
			system("pause");
			return ;
		}
		++q; 
	}
	if(q>=50)
		{
			printf("查无此人！");
			system("pause");
			return;
		}
}
void change(men* p)
{
	int s;
	int ret;
	printf("请输入操作位置：");
	scanf("%d", &s);
	ret = find(p, s);
	if (ret != -1)
	{
		int j = ret;
		for (; j < n; j++)
		{
			p->list[j] = p->list[j + 1];
		}
		printf("已删除！！！\n请重新输入\n");
		printf("请输入联系人姓名：");
		scanf("%s", p->list[j].name);
		printf("请输入联系人电话：");
		scanf("%s", &p->list[j].phone);
	}
}
void sort_name(men* p)
{
	struct man temp;
	int y = 0;
	printf("请选择排序的方式\n1）编号排序  2）姓名排序");
	scanf("%d", &y);
	switch (y)
	{
	case 1:
		for (int i = 0; i < n - 1; i++)
		{
			for (int j = 0; j < n- 1 - i; j++)
			{
				if (p->list[j].num > p->list[j + 1].num)
				{
					temp = p->list[j];
					p->list[j] = p->list[j + 1];
					p->list[j + 1] = temp;
				}
			}
		}
		break;
	case 2:
		for (int i = 0; i < n - 1; i++)
		{
			for (int j = 0; j < n - 1 - i; j++)
			{
				if (strcmp(p->list[j].name, p->list[j + 1].name) > 0)
				{
					temp = p->list[j];
					p->list[j] = p->list[j + 1];
					p->list[j + 1] = temp;
				}
			}
		}
		break;
	default:
		printf("错误操作请重新输入");
		getchar();
		system("cls");
		break;
	}
}
